from __future__ import unicode_literals

__author__ = '{{ cookiecutter.full_name }}'
__copyright__ = 'Copyright {{ cookiecutter.year }} {{ cookiecutter.full_name }}'
__license__ = 'BSD'
__title__ = '{{ cookiecutter.project_name }}'
__version__ = '{{ cookiecutter.version }}'

default_app_config = '{{ cookiecutter.app_name }}.apps.DocumentExtraDataApp'
