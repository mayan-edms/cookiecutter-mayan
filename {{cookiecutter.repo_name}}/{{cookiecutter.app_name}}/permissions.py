from __future__ import absolute_import, unicode_literals

from django.utils.translation import ugettext_lazy as _

from permissions import PermissionNamespace

# Before defining their own permissions, each app must create at least one
# permission namespace.
namespace = PermissionNamespace(
    '{{ cookiecutter.app_name }}',
    _('{{ cookiecutter.project_short_description }}')
)

# Add permissions to a permission namespace.
# Create permissions for each action administrative action.
# Do no assume that the permission for creating an object will also allow
# viewing that object. Permissions should be as atomic as possible.
# For example:
# - one for creating and object
# - one for for editing
# - one for deleting
# - one for viewing

# Follow variable naming conventions.
permission_extra_data_view = namespace.add_permission(
    name='extra_data_view', label=_('View document extra data')
)
